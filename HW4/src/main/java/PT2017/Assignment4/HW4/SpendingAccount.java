package PT2017.Assignment4.HW4;

import java.io.Serializable;
import java.util.Random;

public class SpendingAccount extends Account  implements Serializable {

	private static final long serialVersionUID = 1L;
	public SpendingAccount(int pID){
		super("spe"+setId(),pID);
	}
	public SpendingAccount(int sum,int pID){
		super("spe"+setId(),sum,pID);
	}
	private static int setId(){
		Random f=new Random();
		return f.nextInt(99999-10000+1)+10000;
	}
}
