package PT2017.Assignment4.HW4;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Observable;
import java.util.Random;
//import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Account extends Observable implements Serializable {

private static final long serialVersionUID = 1L;
private String id;
private int persID;
private int amount;
private Date dateCreated;

public Account(String id, int amount,int persID){  //used for saving account
	this.id=id;
	this.amount=amount;
	Calendar today = Calendar.getInstance();
	today.clear(Calendar.HOUR); today.clear(Calendar.MINUTE); today.clear(Calendar.SECOND);
	dateCreated = today.getTime();
	this.persID=persID;
}
public Account(String id,int persID){   //used for spending accounts and initializing saving account;s constructor
	this.id=id;
	amount=new Random().nextInt(1000);
	dateCreated=setDate();
	this.persID=persID;
}

public int getPersID(){
	return persID;
}
public int getAmount() {
	return amount;
}
private Date setDate(){
	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	Date d1,d2;
	try {
		d1 = sdf.parse("01-01-2000");
		d2 = sdf.parse("30-04-2017");
		long random = ThreadLocalRandom.current().nextLong(d1.getTime(), d2.getTime());
		return new Date(random);
	} catch (ParseException e) {
		e.printStackTrace();
	}
	return new Date(); 
}
public String getDate() {
	SimpleDateFormat spf = new SimpleDateFormat("dd/MM/yyyy");
	return spf.format(dateCreated);	
}
public void setAmount(int amount) {
	assert amount >= 0 : "Invalid amount";
	this.amount = amount;
}
public String getId() {
	return id;
}
public void withdraw(int sum){
	assert amount >= sum : "There is not enough money";
	amount-=sum;
	assert amount == amount - sum : "Error during withraw";
	setChanged();
	notifyObservers("You extracted "+sum+" from account " +id );
}
public void withdraw(){
	amount=0;
	setChanged();
	notifyObservers("You extracted all money from account " +id );
}
public void edit_amount(int value){
	assert value >= 0 : "Invalid value";
	amount=value;
	assert amount == value : "There was an error during amount editing";
	setChanged();
	notifyObservers("Account " +id +"has been editted");
}
public void add_money(int value){
	assert value >= 0 : "Invalid value";
	amount+=value;
	assert amount == amount + value : "There was an error during deposit";
	setChanged();
	notifyObservers("Account " +id +"has been added"+value +"amount of money");
}
}
