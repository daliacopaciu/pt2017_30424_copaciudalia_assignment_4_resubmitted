package PT2017.Assignment4.HW4;

import java.io.Serializable;
import java.util.*;

public class SavingAccount extends Account implements Serializable {
private double interest;
private int deposit_period;  //measured in months
private static final long serialVersionUID = 1L;

public SavingAccount(int pID){
	super("sav"+setId(),pID);
	deposit_period=new Random().nextInt(100);
	interest=0.98/100 * super.getAmount()*deposit_period+super.getAmount();
}
public SavingAccount(int amount, int period,int pID){
	super("sav"+setId(),amount,pID);
	this.deposit_period=period;
	interest=0.98/100 * super.getAmount()*deposit_period+super.getAmount();
}
private static int setId(){
	Random f=new Random();
	return f.nextInt(99999-10000+1)+10000;
}
public double getInterest(){
	return interest;
}
public int getDepositPeriod(){
	return  deposit_period;
}
public void editDepositPeriod(int period){
	if(period>0)  // money should stay at least a month    
		deposit_period=period;
	else throw new IllegalArgumentException("The deposit period(months) should be a positive integer"); 
}
}
