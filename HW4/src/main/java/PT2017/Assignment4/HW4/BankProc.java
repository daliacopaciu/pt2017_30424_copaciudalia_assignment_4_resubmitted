package PT2017.Assignment4.HW4;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface BankProc {
	
	/**
	 * Adds a new person to the person list
	 * @pre p!=null
	 * @post person.size()==person.size()@pre+1
	 * @post person.contains(p)
	 */
	public void add_person(Person p);
	
	/**
	 * Removes a person from person list, and all its existing accounts
	 * @pre p!=null && person.contains(p)
	 * @post person.size()==person.size()@pre-1
	 * @post !person.contains(p) 
	 * @param P
	 */
	public void remove_person(Person P);
	
	/**
	 * Adds a new account with owner a
	 * @pre p!=null && a!=null && person.contains(p)
	 * @post all.get(p.getID()).contains(a)
	 * @param p
	 * @param a
	 */
	public void add_account(Person p,Account a);
	
	/**
	 * Removes an account with its id and its owner's id given
	 * @pre getAccounts().keySet().contains(pid) 
	 * @post getAccounts().size@pre-1
	 * @param id_acc
	 * @param pid
	 */
	public void remove_account(String id_acc,int pid);
	
	/**
	 * Returns person with id given as parameter
	 * @pre getPersons().get(id)!=null
	 * @post @nochange
	 * @param id
	 * @return
	 */
	public Person findPersByID(int id);
	
	/**
	 * Returns account with id given as parameter
	 * @pre accounts.get(id)!=null
	 * @post @nochange
	 * @param id
	 * @return
	 */
	public Account findAccById(String id);
	/**
	 * Returns the existing accounts
	 * @pre accounts.size()>0
	 * @post @nochange
	 * @return
	 */
	public Map<Integer,ArrayList<Account>> getAccounts();
	public void read_accounts();
	public void write_accounts(Map<Integer,ArrayList<Account>> a);
	public void read_persons();
	public void write_persons(List<Person>a);
	/**
	 * Returns the existing persons
	 * @pre person.size()>0
	 * @post @nochange
	 * @return
	 */
	public List<Person> getPersons();
	/**
	 * @pre person.size()>0 && all.size()>0
	 * @return a list of people with their existing accounts
	 */
	public String generateReports();

}
