package PT2017.Assignment4.HW4;

import java.io.*;
import java.util.*;

public class Bank implements BankProc, Serializable {

private static final long serialVersionUID = 1L;
private Map<Integer,ArrayList<Account>> all;
private List<Person> person;
public static final String facc="accounts.txt";
public static final String fpers="persons.txt";
private FileOutputStream fop;
private ObjectOutputStream oop;

private FileOutputStream foa;
private ObjectOutputStream ooa;

private FileInputStream fia;
private ObjectInputStream osia;

private FileInputStream fip;
private ObjectInputStream osip;

public Bank(){
	all=new HashMap<Integer,ArrayList<Account>>();
	person=new ArrayList<Person>();
	
}
/**
 * @invariant isWellFormed()
 */
	public void add_person(Person p){
		assert p != null : "Person object is null";
		assert !(person.contains(p)) : "The person is already registered to the bank";       
		person.add(p);
	}
	/**
	 * @invariant isWellFormed()
	 */
	public void remove_person(Person p){
		assert p != null : "Person object is null";
		assert person.contains(p) : "There is no such person registered";
		all.remove(p.getId());
		person.remove(p);
	}
	/**
	 * @invariant isWellFormed()
	 */
	public void add_account(Person p,Account a){ 
		assert p != null : "Person object is null";
		assert a != null : "Account object is null";
		assert person.contains(p) : "Person is not registered";
		assert all.get(p.getId()).contains(a) : "Account is already opened";
		if(all.get(p.getId())!=null){   //if the list exists
			all.get(p.getId()).add(a);
		} 
		else if(all.get(p.getId())==null){    //if the list does not exist, create a new one
			all.put(p.getId(), new ArrayList<Account>());
			all.get(p.getId()).add(a);
		}
	}
	/**
	 * @invariant isWellFormed()
	 */
	public void remove_account(String id_acc,int id_pers){
		assert id_acc != null : "Invalid account id";
		assert id_pers > 0 : "Invalid person id";
		int counter = 0;
		for(Account acc: all.get(id_pers)){
			if(acc.getId().equals(id_acc)){
				System.out.println("Will delete "+acc.getId());
				all.get(id_pers).remove(counter);
				break;
			}
			counter++;
		}
	}
	
	public Person findPersByID(int id){
		assert id > 0 : "Invalid person id";
		for(Person p:person){
			if(p.getId()==id){
				return p;
			}
		}
		return null;
	}
	
	public Account findAccById(String id){
		assert id != null : "Invalid account id";
		assert id != "" : "Invalid account id";
		for(int key:all.keySet()){
			for(Account lst: all.get(key)){
				if(lst.getId().equals(id))
					return lst;
			}
		}
		return null;
	}
	
	public Map<Integer,ArrayList<Account>> getAccounts(){
		assert all!=null : "Empty bank";
		return all;
	}
	
	@SuppressWarnings("unchecked")
	public void read_accounts(){
		Map<Integer,ArrayList<Account>> list = new HashMap<Integer,ArrayList<Account>>();
		try {
			fia = new FileInputStream(facc);
			osia = new ObjectInputStream(fia);
			list=(HashMap<Integer,ArrayList<Account>>)osia.readObject();
			all=list;
			osia.close();
			fia.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	public void write_accounts(Map<Integer,ArrayList<Account>> a) {
		try {
			foa = new FileOutputStream(facc);
			ooa = new ObjectOutputStream(foa);
			ooa.writeObject(a);
			ooa.close();
			foa.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}	
   }
	@SuppressWarnings("unchecked")
	public void read_persons() {
			List<Person> list = new ArrayList<Person>();
			try {
				fip = new FileInputStream(fpers);
				osip = new ObjectInputStream(fip);
				list=(ArrayList<Person>)osip.readObject();
				person=list;
				osip.close();
				fip.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println(e.getMessage());
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
	}
	public void write_persons(List<Person> a) {
		try {
			fop = new FileOutputStream(fpers);
			oop = new ObjectOutputStream(fop);
			oop.writeObject(a);
			oop.close();
			fop.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}	
		
	}
	
	public List<Person> getPersons(){
		return person;
	}
	
	public String generateReports(){
		String rez="";
		for(Person p:person){
			rez+=p.getName()+"("+p.getId()+")\n";
			List<Account> list=new ArrayList<Account>(all.get(p.getId()));
			int counter=1;
			for(Account a:list){
				rez+="     Account "+counter+" "+a.getId()+"\n";
				counter++;
			}
		}
		return rez;
	}
	
	protected boolean isWellFormed(){
		int m=0,n=0,aux=0;
		if(!person.isEmpty()){
			for(Person p:person){
				n++;
				if(p==null){
					return false;}
				if(all.get(p.getId())!=null)
					aux+=all.get(p.getId()).size();
			}
		}
		else{
			if (all.size() !=0) { //if there are no people, but exist active accounts => mistake
				return false;
			}
		}
		
		if(!all.isEmpty()){
			for(int key:all.keySet()){
				for(Account a:all.get(key)){
					m++;
					if(!all.keySet().contains(a.getPersID())){
						return false;}
				}
			}
		}
		return (n==person.size() && m==aux);
	}
	
	public static void main(String[] args){
		Bank b=new Bank();
//		List<Person>listp=new ArrayList<Person>();
//		Map<Integer,ArrayList<Account>>lista=new HashMap<Integer,ArrayList<Account>>();
//		for(int i=0;i<5;i++){
//			Person p=new Person();
//			listp.add(p);
//			SavingAccount sa=new SavingAccount(p.getId());
//			if(lista.get(p.getId())==null){
//				lista.put(p.getId(), new ArrayList<Account>());
//			}
//			lista.get(p.getId()).add(sa);
//			SpendingAccount sp=new SpendingAccount(p.getId());
//			lista.get(p.getId()).add(sp);
//		}
//		b.write_persons(listp);
//		b.write_accounts(lista);
		b.read_persons();
		b.read_accounts();
		
		for(Person p: b.getPersons()){
			System.out.println(p.getName());
			if(b.getAccounts().get(p.getId())!=null)
			for(Account acc:b.getAccounts().get(p.getId())){
				System.out.println(acc.getId());
			}
		}
		System.out.println(b.isWellFormed());
	}
	
}

	