package PT2017.Assignment4.HW4;

import junit.framework.TestCase;

public class TestUnit extends TestCase {
	private Bank b=new Bank();
	
	public void testAddPerson() {
		
		String name="Dalia Copaciu";
		String job="Maid";
	    Person p=new Person(name,job,"Fantanele");
		b.add_person(p);
		assertEquals(p.toString(),b.getPersons().get(0).toString());
	}
	
	public void testInterest(){
		String name="Dalia Copaciu";
		String job="Maid";
		Person p=new Person(name,job,"Fantanele");
	    b.add_person(p);
		int amount=250;
		int period=4;
		double interest=0.98/100 * amount*period+amount;
		SavingAccount sa=new SavingAccount(amount,period,p.getId());
		b.add_account(p, sa);
		assertEquals(interest,((SavingAccount)b.getAccounts().get(p.getId()).get(0)).getInterest());
	}
	public void testAddMoney(){
		String name="Dalia Copaciu";
		String job="Maid";
		Person p=new Person(name,job,"Fantanele");
	    b.add_person(p);
		int amount=250;
		int newam=280;
		SpendingAccount sa=new SpendingAccount(amount,p.getId());
		b.add_account(p, sa);
		b.getAccounts().get(p.getId()).get(0).add_money(30);
		System.out.println(newam+" "+b.getAccounts().get(p.getId()).get(0).getAmount());
		assertEquals(newam,b.getAccounts().get(p.getId()).get(0).getAmount());
	}
	public void testWithdraw(){
		String name="Dalia Copaciu";
		String job="Maid";
		Person p=new Person(name,job,"Fantanele");
	    b.add_person(p);
		int amount=250;
		int newam=200;
		SpendingAccount sa=new SpendingAccount(amount,p.getId());
		b.add_account(p, sa);
		b.getAccounts().get(p.getId()).get(0).withdraw(50);
		assertEquals(newam,b.getAccounts().get(p.getId()).get(0).getAmount());
	}
	public void testWithdrawSaving(){
		String name="Dalia Copaciu";
		String job="Maid";
		Person p=new Person(name,job,"Fantanele");
	    b.add_person(p);
		int amount=250;
		int newam=0;
		SavingAccount sa=new SavingAccount(amount,4,p.getId());
		b.add_account(p, sa);
		b.getAccounts().get(p.getId()).get(0).withdraw();
		assertEquals(newam,b.getAccounts().get(p.getId()).get(0).getAmount());
	}
}
