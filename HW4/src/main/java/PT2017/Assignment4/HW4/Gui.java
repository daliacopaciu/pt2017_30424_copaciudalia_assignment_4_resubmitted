package PT2017.Assignment4.HW4;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.Font;
import java.util.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;


public class Gui implements Observer {

	private JFrame frame;
	private JTable table;
	private Bank bank;
	private JTextField persname;
	private JTextField job;
	int pointed_person;
	int pointed_field;
	int pointed_acc;
	int pointed_acc_field;
	private JTextField pname;
	private JTable tableacc;
	private JTextField sum1;
	private JTextField period1;
	private JTextField sum2;
	private JComboBox<String> perscombo;
	private JComboBox<String> typecombo;
	private JTextField address;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui window = new Gui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Gui() {
		initialize();
		bank=new Bank();
		bank.read_persons();
		bank.read_accounts();
		updateCombo();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 600, 480);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 11, 574, 389);
		frame.getContentPane().add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Person", null, panel, null);
		panel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		table = new JTable();
		table.addMouseListener(new java.awt.event.MouseAdapter(){
			@Override
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				pointed_person=table.rowAtPoint(evt.getPoint());
				pointed_field=table.columnAtPoint(evt.getPoint());
			}
		});
		scrollPane.setViewportView(table);
		scrollPane.setBounds(131, 11, 417, 175);
		panel.add(scrollPane);
		
		persname = new JTextField();
		persname.setBounds(150, 217, 86, 20);
		panel.add(persname);
		persname.setColumns(10);
		
		job = new JTextField();
		job.setBounds(246, 217, 86, 20);
		panel.add(job);
		job.setColumns(10);
		
		JButton btnAddPerson = new JButton("Add Person");
		btnAddPerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String name=persname.getText();
				String jb=job.getText();
				String addr=address.getText();
				Person p=new Person(name,jb,addr);
				bank.add_person(p);
				setPersonTable();	
				updateCombo();
			}
		});
		btnAddPerson.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		btnAddPerson.setBounds(10, 215, 114, 23);
		panel.add(btnAddPerson);
		
		JButton Show = new JButton("Show persons");
		Show.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		Show.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setPersonTable();
			}
		});
		Show.setBounds(10, 63, 111, 23);
		panel.add(Show);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(150, 203, 46, 14);
		panel.add(lblName);
		
		JLabel lblNewLabel = new JLabel("Job");
		lblNewLabel.setBounds(246, 203, 46, 14);
		panel.add(lblNewLabel);
		
		JButton btnDelete = new JButton("Delete ");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Person pers=bank.getPersons().get(pointed_person);
				if(bank.getAccounts().get(pers.getId())!=null)
					for(Account a: bank.getAccounts().get(pers.getId())){
						bank.remove_account(a.getId(),pers.getId());
					}
				bank.remove_person(pers);
				setPersonTable();
				setAccountTable();
				updateCombo();
			}
		});
		btnDelete.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		btnDelete.setBounds(10, 263, 114, 23);
		panel.add(btnDelete);
	
		pname = new JTextField();
		pname.setBounds(150, 314, 86, 20);
		panel.add(pname);
		pname.setColumns(10);
		
		
		JButton btnEdit = new JButton("Edit");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Person pers=bank.getPersons().get(pointed_person);
				bank.remove_person(pers);
				if(pointed_field==0) 
					show_msg("You cannot edit a person's ID!","Wrong choice!");
				else
					pers.edit(pointed_field, pname.getText());
				bank.add_person(pers);
				//bank.write_persons(pers);
				setPersonTable();
			}
		});
		btnEdit.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		btnEdit.setBounds(10, 312, 114, 23);
		panel.add(btnEdit);
		
		
		JLabel lblName_1 = new JLabel("Edited value");
		lblName_1.setBounds(150, 301, 86, 14);
		panel.add(lblName_1);
		
		address = new JTextField();
		address.setBounds(342, 217, 86, 20);
		panel.add(address);
		address.setColumns(10);
		
		JLabel lblAddress = new JLabel("Address");
		lblAddress.setBounds(343, 203, 85, 14);
		panel.add(lblAddress);
		
		
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Accounts", null, panel_1, null);
		panel_1.setLayout(null);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(69, 41, 490, 143);
		panel_1.add(scrollPane_1);
		
		tableacc = new JTable();
		tableacc.addMouseListener(new java.awt.event.MouseAdapter(){
		public void mouseClicked(java.awt.event.MouseEvent evt) {
				pointed_acc=tableacc.rowAtPoint(evt.getPoint());
				pointed_acc_field=tableacc.columnAtPoint(evt.getPoint());
			}
		});
		scrollPane_1.setViewportView(tableacc);
		
		JButton btnShowAccounts = new JButton("Show Accounts");
		btnShowAccounts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setAccountTable();
			}
		});
		btnShowAccounts.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		btnShowAccounts.setBounds(10, 11, 125, 23);
		panel_1.add(btnShowAccounts);
		
		JButton btnAddAccount = new JButton("Add Account");
		btnAddAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int persID,sum,period;
				try{
					persID=Integer.parseInt(perscombo.getSelectedItem().toString().split(" ")[2]);  //person id
					sum=Integer.parseInt(sum1.getText());
					String type=typecombo.getSelectedItem().toString();
					if(type.equals("Saving Account")){
						period=Integer.parseInt(period1.getText());
						SavingAccount sa=new SavingAccount(sum,period,persID);
						bank.add_account(bank.findPersByID(persID), sa);
					}
					else {
						SpendingAccount sp=new SpendingAccount(sum,persID);
						bank.add_account(bank.findPersByID(persID), sp);
						System.out.println("Adding account "+sp.getAmount());
						//bank.write_accounts(sp);
					}
					setAccountTable();
					updateCombo();
				}
				catch(IllegalArgumentException e1){
					show_msg(e1.getLocalizedMessage(),"Error");
				}
			}
		});
		btnAddAccount.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		btnAddAccount.setBounds(10, 205, 120, 23);
		panel_1.add(btnAddAccount);
		perscombo = new JComboBox<String>();
		perscombo.setBounds(140, 207, 157, 20);
		panel_1.add(perscombo);
		
		JLabel lblPerson = new JLabel("Person");
		lblPerson.setBounds(140, 195, 46, 14);
		panel_1.add(lblPerson);
		
		typecombo = new JComboBox<String>();
		typecombo.setBounds(307, 207, 91, 20);
		panel_1.add(typecombo);
		
		JLabel lblType = new JLabel("Type");
		lblType.setBounds(307, 195, 46, 14);
		panel_1.add(lblType);
		
		sum1 = new JTextField();
		sum1.setBounds(410, 207, 63, 20);
		panel_1.add(sum1);
		sum1.setColumns(10);
		
		JLabel lblDepositPeriod = new JLabel("Deposit period");
		lblDepositPeriod.setBounds(483, 195, 86, 14);
		panel_1.add(lblDepositPeriod);
		
		JButton btnDelete_1 = new JButton("Delete");
		btnDelete_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
				String o=(tableacc.getModel().getValueAt(pointed_acc, 1)).toString();
				String idacc=(tableacc.getModel().getValueAt(pointed_acc, 0)).toString();
				int idpers=Integer.parseInt(o);
				System.out.println(idpers+" "+idacc);
				bank.remove_account(idacc,idpers);
				for(Account a:bank.getAccounts().get(idpers)){
					System.out.println(a.getId());
				}
				updateCombo();
				setAccountTable();
				}
				catch(Exception e5){
					show_msg(e5.getLocalizedMessage(),"Error");
				}
			}
		});
		btnDelete_1.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		btnDelete_1.setBounds(10, 239, 120, 23);
		panel_1.add(btnDelete_1);
		
		JButton btnEdit_1 = new JButton("Edit");
		btnEdit_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					int value=Integer.parseInt(sum2.getText());
					String idacc=(tableacc.getModel().getValueAt(pointed_acc, 0)).toString();
					Account a=bank.findAccById(idacc);
					if(pointed_acc_field!=2 && pointed_acc_field!=4) 
						show_msg("You cannot edit this field!","Wrong choice!");
					else if(value<0)
						show_msg("The value must be a positive integer!","Wrong choice!");
					else{
						if(pointed_acc_field==2)  //amount 
							a.edit_amount(value);
						else ((SavingAccount)a).editDepositPeriod(value);
						setAccountTable();
					}
			    }
				catch(Exception ee){
					show_msg(ee.getLocalizedMessage(),"Wrong");
				}
			}
		});
		btnEdit_1.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		btnEdit_1.setBounds(10, 273, 120, 23);
		panel_1.add(btnEdit_1);
		
		JButton btnAddMoney = new JButton("Add Money for Spending Account");
		btnAddMoney.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String idacc=(tableacc.getModel().getValueAt(pointed_acc, 0)).toString();
				if(idacc.substring(0,3).equals("sav")){
					show_msg("You cannot add money twice to the saving account","Wrong");
				}
				else{
					Account a=bank.findAccById(idacc);
					a.add_money(Integer.parseInt(sum2.getText()));
					setAccountTable();
				}
			}
		});
		btnAddMoney.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		btnAddMoney.setBounds(10, 307, 253, 23);
		panel_1.add(btnAddMoney);
		
		JButton btnWithdraw = new JButton("Withdraw");
		btnWithdraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String idacc=(tableacc.getModel().getValueAt(pointed_acc, 0)).toString();
				Account a = bank.findAccById(idacc);
				if(idacc.substring(0,3).equals("sav")){
					a.withdraw();
				}
				else{
					try{
						if(Integer.parseInt(sum2.getText())>a.getAmount())
							show_msg("The value is too large for the existing amount of money","Too much");
						else
							a.withdraw(Integer.parseInt(sum2.getText()));
					}catch(Exception e10){
						show_msg(e10.getLocalizedMessage(),"Lack");
					}
				}
				setAccountTable();
			}
		});
		btnWithdraw.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		btnWithdraw.setBounds(10, 338, 120, 23);
		panel_1.add(btnWithdraw);
		
		period1 = new JTextField();
		period1.setBounds(479, 207, 66, 20);
		panel_1.add(period1);
		period1.setColumns(10);
		
		JLabel lblSum = new JLabel("Sum");
		lblSum.setBounds(411, 195, 46, 14);
		panel_1.add(lblSum);
		
		sum2 = new JTextField();
		sum2.setBounds(283, 294, 86, 20);
		panel_1.add(sum2);
		sum2.setColumns(10);
		
		JLabel lblSum_1 = new JLabel("Sum or deposit period");
		lblSum_1.setBounds(284, 278, 133, 14);
		panel_1.add(lblSum_1);
		
		JButton btnGetReport = new JButton("Get Report");
		btnGetReport.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		btnGetReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				show_msg(bank.generateReports(),"Reports");
			}
		});
		btnGetReport.setBounds(163, 12, 110, 23);
		panel_1.add(btnGetReport);
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				bank.write_persons(bank.getPersons());
				bank.write_accounts(bank.getAccounts());
			}
		});
		btnSave.setBounds(237, 411, 89, 23);
		frame.getContentPane().add(btnSave);
		typecombo.addItem("Saving Account");
		typecombo.addItem("Spending Account");
	}
	private void setPersonTable(){
		DefaultTableModel dtm=new DefaultTableModel();
		dtm.addColumn("Id");
		dtm.addColumn("Name");
		dtm.addColumn("Job");
		dtm.addColumn("Address");
		for(Person p: bank.getPersons()){
			dtm.addRow(new Object[] {p.getId(),p.getName(),p.getJob(),p.getAddress()});
		}
		table.setModel(dtm);
	}
	private void setAccountTable(){
		DefaultTableModel dtm=new DefaultTableModel();
		dtm.addColumn("Id");
		dtm.addColumn("PersonID");
		dtm.addColumn("Amount");
		dtm.addColumn("DateCreated");
		dtm.addColumn("DepositPeriod");
		dtm.addColumn("Interest");
		int deposit;
		double interest;
		for(int key:bank.getAccounts().keySet())
			for(Account a: bank.getAccounts().get(key)){
				a.addObserver(this);
				if(a instanceof SavingAccount){
					deposit=((SavingAccount)a).getDepositPeriod();
					interest=((SavingAccount)a).getInterest();
				}
				else {
					deposit=0;
					interest=0;
				}
				dtm.addRow(new Object[] {a.getId(),key,a.getAmount(),a.getDate(),deposit,interest});
			}
		tableacc.setModel(dtm);
	}
	private void updateCombo(){
		List<Person>list=new ArrayList<Person>();
		list.addAll(bank.getPersons());
		perscombo.removeAll();
		for (Person item : list) {
		    perscombo.addItem(item.getName()+" "+item.getId());
		}
	
	}
	private void show_msg(String msg, String title)
    {
        JOptionPane.showMessageDialog(table, msg, title, JOptionPane.INFORMATION_MESSAGE);
    }

	public void update(Observable o, Object arg) {
		show_msg(arg.toString(),"Information for person");
		
	}
}
