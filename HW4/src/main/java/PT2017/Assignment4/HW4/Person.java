package PT2017.Assignment4.HW4;

import java.io.Serializable;
import java.util.*;
public class Person implements Serializable {

	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private String job;
	private String address;

	public Person() {
		Random f=new Random();
		id=f.nextInt(99999-10000+1)+10000;  //id =random nr of 5 digits
		name=createnamesAndJobs('n');
		job=createnamesAndJobs('j');
		address=createAddress();
	}
	public Person(String name,String job,String address) {
		Random f=new Random();
		id=f.nextInt(99999-10000+1)+10000;  //id =random nr of 5 digits
		this.name=name;
		this.job=job;
		this.address=address;
	}
	public String createAddress(){
		String[] s=new String[]{
				"7217 Bank St. Parlin",
				"474 Oak Ave", 
				"40 Titusville",
				"30 Glen Eagles",
				"717 Evergreen Street", 
				"821 Gonzales Rd",
				"5 Hall Ave",
				"4 Santa Clara",
				"7713 South Nicolls Street",
				"7689 Flemington",
				"326 Olive Stillwater", 
				"13 Brown Street" ,
				"417 Oklahoma St.",
				"8960 Addison St." };
		int index=new Random().nextInt(14);
		return s[index];
	}
	public String createnamesAndJobs(char c){
		
	      String[] s=new String[]{"Gordon Tomberlin",
			  "Dewitt Steppe",
			  "Estella Brashears",
			  "Pauline Cancel",
			  "Gwen Nero",
			  "Tyron Alatorre",
			  "Kiana Tait",
			  "Tomika Wyland",
			  "Classie Overson",
			  "Layla Treese",
			  "Carmela Bulow",
			  "Yong Labrecque",
			  "Vickey Greenfield",
			  "Sudie Greathouse",
			  "Refugio Leech",
			  "Leona Spoto",
			  "Markus Nicolson",
			  "Jamey Hesse",
			  "Steve Yoshioka",
			  "Miyoko Stradford"};
      
	      String[] j=new String[]{"Secretary", "Barber","Engineer","Astrologer","Builder","Reporter","Sailor","Architect","Pilot","Handyman","Gardener","Chef","Courier","Teacher","Doctor","Nurse","Model","Policeman","Farmer","Housekeeper"};
	      Random f=new Random();
		  int index=f.nextInt(20);
		  String chosen;
		  if(c=='n')
			  chosen=s[index];
		  else chosen=j[index];
		  return chosen;
	      
	}
	
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getJob() {
		return job;
	}
	public String getAddress(){
		return address;
	}
	public void edit(int fieldnr,String value){
		switch(fieldnr){
		case 1: 
			name=value;
			break;
		case 2:
			job=value;
			break;
		case 3:
			address=value;
			break;
		default: break;
		}
	}
	
}
